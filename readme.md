# People counting using a USB camera

This sample uses ViFlow and DeepStream plugins to count people using a USB camera.

### Dependencies

- [CMake](https://cmake.org/install/)
- [ViFlow](https://gitlab.com/aiva.vision/cxx-development/viflow)
- [GStreamer](https://gstreamer.freedesktop.org/documentation/installing/on-linux.html?gi-language=c)
- [DeepStream](https://docs.nvidia.com/metropolis/deepstream/dev-guide/text/DS_Overview.html)
- [GLib](https://developer.gnome.org/glib/)
- [GObject](https://developer.gnome.org/gobject/)
- [libconfig](https://hyperrealm.github.io/libconfig/)
- [spdlog](https://github.com/gabime/spdlog)

To install some of the prerequisites, execute the following command:

```bash
sudo apt-get install \
    libgstreamer-plugins-base1.0-dev \
    libgstreamer-plugins-good1.0-dev \
    libgstrtspserver-1.0-dev \
    libgstreamer1.0-dev
```

For the rest, try using the scripts from [this repository](https://gitlab.com/aiva.vision/devops/packages).

### Install

1. Clone the repository.

   ```bash
   git clone git@gitlab.com:aiva.vision/cxx-development/viflow-usb-people-count.git
   ```

2. Build the project and install:

   ```bash
   mkdir -p viflow-usb-people-count/release

   cmake -S viflow-usb-people-count -B viflow-usb-people-count/release -D CMAKE_BUILD_TYPE=Release

   cmake --build viflow-usb-people-count/release -j

   cmake --install viflow-usb-people-count/release
   ```

### Run

Copy Peoplenet to the DeepStream models path.

```bash
sudo cp -r ./models/* /opt/nvidia/deepstream/deepstream/samples/models
```

Finally, execute the binary:

```bash
people-count ./configs/<usecase>/main.cfg
```

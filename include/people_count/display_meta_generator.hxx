#pragma once

#include <gstnvdsmeta.h>
#include <viflow/app_transform.hxx>

namespace people_count {
/*
   Generates display meta for every buffer.
   */
class Display_Meta_Generator : public viflow::App_Transform {
public:
  static unsigned pgie_person_class_id;

private:
  Display_Meta_Generator(const std::string &name);

  void process(viflow::Buffer &buf);

  void add_person_count(NvDsBatchMeta *batch_meta, NvDsFrameMeta *frame_meta,
                        unsigned person_count);

  friend class viflow::Element;
};

} // namespace people_count

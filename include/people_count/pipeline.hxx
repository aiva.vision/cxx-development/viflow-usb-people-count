#pragma once

#include <filesystem>
#include <libconfig.h++>
#include <viflow/pipeline.hxx>

namespace people_count {

namespace Pipeline_Props {

extern const char *camera_id;
extern const char *inference_engine_config;
extern const char *tracker_config;
extern const char *analytics_config;

} // namespace Pipeline_Props

class Pipeline : public viflow::Pipeline {
public:
  static const std::string &name;

  static viflow::Shared_Pipeline instance();

private:
  /*
     Builds the pipeline from the config.
     */
  Pipeline(const std::filesystem::path &config);

  void add_source(const libconfig::Setting &cfg);
  void append_stream_muxer();
  void append_inference_engine(const libconfig::Setting &config);
  void append_tracker(const libconfig::Setting &config);
  void append_analytics(const libconfig::Setting &config);
  void append_osd_sink();

  friend class viflow::Element;
};

} // namespace people_count

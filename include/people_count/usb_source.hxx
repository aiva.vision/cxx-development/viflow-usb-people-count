#pragma once

#include <libconfig.h++>
#include <viflow/bin.hxx>

namespace people_count {

class USB_Source : public viflow::Bin {
private:
  /*
     Creates a named element.
     */
  USB_Source(const std::string &name, const std::string &device_path);

  friend class viflow::Element;
};

} // namespace people_count

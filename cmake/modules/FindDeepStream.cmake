message(STATUS "In DeepStream find module.")
list(APPEND CMAKE_MESSAGE_INDENT "  ")

set(DeepStream_Path "/opt/nvidia/deepstream/deepstream"
  CACHE PATH "DeepStream install location.")
list(APPEND CMAKE_PREFIX_PATH ${DeepStream_Path})

add_library(deepstream INTERFACE IMPORTED GLOBAL)
add_library(nvds::deepstream ALIAS deepstream)

# Searches for the library, adds it as an IMPORTED
# library and adds it to the DeepStream INTERFACE.
function(import_ds_library library_name)

  find_library(${library_name}_LOCATION NAMES ${library_name} REQUIRED)

  add_library(${library_name} SHARED IMPORTED GLOBAL)
  set_target_properties(${library_name} PROPERTIES
    IMPORTED_LOCATION ${${library_name}_LOCATION})
  target_link_options(${library_name} INTERFACE
    LINKER:-rpath=${DeepStream_Path}/lib)
  target_include_directories(${library_name} INTERFACE
    ${DeepStream_Path}/sources/includes)

  target_link_libraries(deepstream INTERFACE ${library_name})

  add_library(nvds::${library_name} ALIAS ${library_name})

  message(VERBOSE "Successfully imported ${library_name}.")

endfunction()

import_ds_library(nvds_utils)
import_ds_library(nvds_meta)
import_ds_library(nvdsgst_meta)
import_ds_library(nvdsgst_helper)
import_ds_library(nvdsgst_smartrecord)
import_ds_library(nvbufsurftransform)
import_ds_library(nvbufsurface)

list(POP_BACK CMAKE_MESSAGE_INDENT)

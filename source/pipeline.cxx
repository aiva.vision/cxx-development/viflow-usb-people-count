#include <spdlog/spdlog.h>
#include <string>
#include <viflow/app_sink.hxx>
#include <viflow/tee.hxx>
#include <viflow/uri_decode_bin.hxx>

#include "people_count/bus_listener.hxx"
#include "people_count/display_meta_generator.hxx"
#include "people_count/pipeline.hxx"
#include "people_count/usb_source.hxx"

const char *people_count::Pipeline_Props::camera_id = "camera-id";
const char *people_count::Pipeline_Props::inference_engine_config =
    "inference-engine-config";
const char *people_count::Pipeline_Props::tracker_config = "tracker-config";
const char *people_count::Pipeline_Props::analytics_config = "analytics-config";

static std::string get_abs_path(const libconfig::Setting &setting,
                                const std::string &field) {

  std::filesystem::path p;
  p = setting[field];

  if (p.is_absolute()) {
    return p;
  }
  auto config_path = std::filesystem::absolute(setting.getSourceFile());
  return config_path.parent_path() / p;
}

const std::string &people_count::Pipeline::name = "people_count_pipeline";

viflow::Shared_Pipeline people_count::Pipeline::instance() {
  return viflow::Element::instance<viflow::Pipeline>(name);
}

people_count::Pipeline::Pipeline(const std::filesystem::path &config)
    : viflow::Pipeline(name) {
  /*
     Parse the config file.
     */
  libconfig::Config cfg;
  try {
    cfg.readFile(config.c_str());

  } catch (const libconfig::FileIOException &e) {

    throw std::runtime_error{
        std::string{"Failed to open the config file from: "} + config.string()};
  }

  libconfig::Setting &conf_root = cfg.getRoot();
  /*
     Add and configure the elements.
     */
  add_source(conf_root);
  append_stream_muxer();
  append_inference_engine(conf_root);

  if (conf_root.exists(Pipeline_Props::tracker_config)) {
    append_tracker(conf_root);
  }
  if (conf_root.exists(Pipeline_Props::analytics_config)) {
    append_analytics(conf_root);
  }
  append_osd_sink();
  /*
     Register a bus watch.
     */
  get_bus()->add_watch(std::make_unique<Bus_Listener>());
}

void people_count::Pipeline::add_source(const libconfig::Setting &cfg) {

  unsigned device_id = cfg[Pipeline_Props::camera_id];
  auto device_path = "/dev/video" + std::to_string(device_id);

  add<USB_Source>(name + "_source", device_path);
}

void people_count::Pipeline::append_stream_muxer() {

  auto source = last_added();
  auto muxer = add("nvstreammux", name + "_stream_muxer");

  muxer->set_property("batch-size", 1);
  muxer->set_property("width", 1920);
  muxer->set_property("height", 1080);
  muxer->set_property("nvbuf-memory-type", 0);
  muxer->set_property("batched-push-timeout", 40000);
  muxer->set_property("live-source", true);

  source->link(muxer, "src", "sink_0");
}

void people_count::Pipeline::append_inference_engine(
    const libconfig::Setting &config) {

  auto infer_config =
      get_abs_path(config, Pipeline_Props::inference_engine_config);

  auto queue = append("queue", name + "_inference_engine_queue");
  auto engine = append("nvinfer", name + "_inference_engine");

  engine->set_property("batch-size", 1);
  engine->set_property("config-file-path", infer_config.c_str());
}

void people_count::Pipeline::append_tracker(const libconfig::Setting &config) {

  auto tracker_config = get_abs_path(config, Pipeline_Props::tracker_config);

  auto queue = append("queue", name + "_tracker_queue");
  auto tracker = append("nvtracker", name + "_tracker");

  tracker->set_property(
      "ll-lib-file",
      "/opt/nvidia/deepstream/deepstream/lib/libnvds_nvmultiobjecttracker.so");
  tracker->set_property("ll-config-file", tracker_config.c_str());
}

void people_count::Pipeline::append_analytics(
    const libconfig::Setting &config) {

  auto analytics_config =
      get_abs_path(config, Pipeline_Props::analytics_config);

  auto queue = append("queue", name + "_analytics_queue");
  auto analytics = append("nvdsanalytics", name + "_analytics");

  analytics->set_property("config-file", analytics_config.c_str());
}

void people_count::Pipeline::append_osd_sink() {

  append<Display_Meta_Generator>(name + "_meta_generator");
  append("queue", name + "_osd_queue");
  append("nvdsosd", name + "_osd");
  append("nvvideoconvert", name + "_nv_video_converter");

  auto egl_sink = append("glimagesink", name + "_gl_sink");

  egl_sink->set_property("enable-last-sample", false);
  egl_sink->set_property("sync", false);
  egl_sink->set_property("qos", false);
}

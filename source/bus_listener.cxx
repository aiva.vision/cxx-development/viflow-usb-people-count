#include <gst/gstbin.h>
#include <gst/gstdebugutils.h>
#include <spdlog/spdlog.h>
#include <viflow/main_loop.hxx>

#include "people_count/bus_listener.hxx"
#include "people_count/pipeline.hxx"

bool people_count::Bus_Listener::process_message(
    const viflow::Bus &bus, const viflow::Message &message) {

  switch (message.get_type()) {
  case GST_MESSAGE_INFO:
    process_info(message);
    break;
  case GST_MESSAGE_WARNING:
    process_warning(message);
    break;
  case GST_MESSAGE_ERROR:
    process_error(message);
    break;
  case GST_MESSAGE_STATE_CHANGED:
    process_state_changed(message);
    break;
  case GST_MESSAGE_EOS:
    process_eos(message);
    break;
  default:
    break;
  }
  return true;
}

void people_count::Bus_Listener::process_info(const viflow::Message &message) {

  auto context = message.parse_info();
  spdlog::info("Received a message from the '{}' element with the content '{}'",
               message.get_source_name(), context.error.message());
}

void people_count::Bus_Listener::process_warning(
    const viflow::Message &message) {

  auto context = message.parse_warning();
  spdlog::warn("Received a message from the '{}' element with the content '{}'",
               message.get_source_name(), context.error.message());
}

void people_count::Bus_Listener::process_error(const viflow::Message &message) {

  auto context = message.parse_error();
  spdlog::error(
      "Received a message from the '{}' element with the content '{}'",
      message.get_source_name(), context.error.message());

  Pipeline::instance()->stop();
  viflow::Main_Loop::instance()->quit();
}

void people_count::Bus_Listener::process_state_changed(
    const viflow::Message &message) {

  if (message.get_source_name() == Pipeline::name) {

    auto pipeline = Pipeline::instance();
    auto context = message.parse_state_changed();

    switch (context.new_state) {
    case GST_STATE_PLAYING:
      spdlog::info("The pipeline is playing.");
      GST_DEBUG_BIN_TO_DOT_FILE(GST_BIN(pipeline->get_core()),
                                GST_DEBUG_GRAPH_SHOW_ALL,
                                "people_count-playing");
      break;
    case GST_STATE_PAUSED:
      if (context.old_state == GST_STATE_PLAYING) {
        spdlog::info("The pipeline is paused.");
      }
      break;
    case GST_STATE_READY:
      GST_DEBUG_BIN_TO_DOT_FILE(GST_BIN(pipeline->get_core()),
                                GST_DEBUG_GRAPH_SHOW_ALL, "people_count-ready");
      if (context.old_state == GST_STATE_NULL) {
        spdlog::info("The pipeline is ready.");
      } else {
        spdlog::info("The pipeline is stopped.");
      }
      break;
    default:
      break;
    }
  }
}

void people_count::Bus_Listener::process_eos(const viflow::Message &message) {

  spdlog::info("End-Of-Stream reached.");

  Pipeline::instance()->stop();
  viflow::Main_Loop::instance()->quit();
}

#include <viflow/queue.hxx>
#include <viflow/uri_decode_bin.hxx>

#include "people_count/usb_source.hxx"

people_count::USB_Source::USB_Source(const std::string &name,
                                     const std::string &device_path)
    : Bin{name} {

  auto src = add("v4l2src", name + "_v4l2_src");
  auto parser = append("jpegparse", name + "_parser");
  auto filter = append("capsfilter", name + "_filter");
  auto queue = append("queue", name + "_queue");
  auto decoder = append("nvv4l2decoder", name + "_decoder");
  auto converter = append("nvvideoconvert", name + "_converter");
  /*
     Configure the elements.
     */
  src->set_property("device", device_path.c_str());

  viflow::Capabilities caps{gst_caps_new_simple(
      "image/jpeg", "framerate", GST_TYPE_FRACTION, 30, 1, nullptr)};
  filter->set_property("caps", caps.get_core());

  queue->set_property("max-size-time", 1000000000);
  queue->set_property("leaky", 2);

  decoder->set_property("cudadec-memtype", 0);
  decoder->set_property("mjpeg", true);
  /*
     Finally set the appropriate ghost pads.
     */
  set_src_elem(name + "_converter");
}

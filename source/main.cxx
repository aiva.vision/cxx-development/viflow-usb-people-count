#include <gst/gst.h>
#include <gst/gstevent.h>
#include <spdlog/spdlog.h>
#include <viflow/main_loop.hxx>

#include "people_count/pipeline.hxx"

GST_DEBUG_CATEGORY_STATIC(people_count_category);
#define GST_CAT_DEFAULT people_count_category

void stop(int signal_number) {

  spdlog::info("Caught signal with ID {}, stopping the pipeline...",
               signal_number);
  /*
     Send an EOS event and let the bus listener handle it.
     */
  people_count::Pipeline::instance()->stop();
  viflow::Main_Loop::instance()->quit();
}

int main(int argc, char *argv[]) {
  /*
     Register CTRL-C handler.
     */
  signal(SIGINT, stop);
  /*
     Initialize GStreamer.
     */
  gst_init(0, nullptr);
  GST_DEBUG_CATEGORY_INIT(people_count_category, "people_count", 0,
                          "people_count debug category.");
  /*
     Make sure a config is passed.
     */
  if (argc != 2) {
    spdlog::error("Please provide a config!");
  }
  auto pipeline =
      viflow::Element::register_instance<people_count::Pipeline>(argv[1]);
  /*
     Set the pipeline to PLAYING.
     */
  pipeline->play();
  /*
     Run the main loop.
     */
  auto &main_loop = viflow::Main_Loop::register_instance();
  main_loop->run();
  /*
     Leave the rest to RAII.
     */
  spdlog::info("Exited gracefully.");
  return EXIT_SUCCESS;
}

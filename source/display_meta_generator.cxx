#include <cstring>
#include <spdlog/spdlog.h>
#include <string>

#include "people_count/display_meta_generator.hxx"

#define PGIE_CLASS_ID_PERSON 0

unsigned people_count::Display_Meta_Generator::pgie_person_class_id = 0;

people_count::Display_Meta_Generator::Display_Meta_Generator(
    const std::string &name)
    : viflow::App_Transform{name} {}

void people_count::Display_Meta_Generator::process(viflow::Buffer &buf) {

  guint frame_number = 0;
  guint person_count = 0;
  guint num_rects = 0;

  auto batch_meta = gst_buffer_get_nvds_batch_meta(buf.get_core());

  for (auto l_frame = batch_meta->frame_meta_list; l_frame != nullptr;
       l_frame = l_frame->next) {

    auto frame_meta = static_cast<NvDsFrameMeta *>(l_frame->data);
    for (auto l_obj = frame_meta->obj_meta_list; l_obj != nullptr;
         l_obj = l_obj->next) {
      auto obj_meta = static_cast<NvDsObjectMeta *>(l_obj->data);
      if (obj_meta->class_id == PGIE_CLASS_ID_PERSON) {
        person_count++;
        num_rects++;
      }
    }
    add_person_count(batch_meta, frame_meta, person_count);
  }
  spdlog::info("Frame Number = {} Number of objects = {} Person Count = {}",
               frame_number, num_rects, person_count);

  frame_number++;
}

void people_count::Display_Meta_Generator::add_person_count(
    NvDsBatchMeta *batch_meta, NvDsFrameMeta *frame_meta,
    unsigned person_count) {

  auto display_meta = nvds_acquire_display_meta_from_pool(batch_meta);
  auto txt_params = &display_meta->text_params[0];

  display_meta->num_labels = 1;

  auto text = "Person = " + std::to_string(person_count);
  txt_params->display_text = static_cast<char *>(g_malloc0(text.size() + 1));
  strcpy(txt_params->display_text, text.c_str());

  // Now set the offsets where the string should appear
  txt_params->x_offset = 10;
  txt_params->y_offset = 12;

  // Font , font-color and font-size
  txt_params->font_params.font_name = const_cast<char *>("Serif");
  txt_params->font_params.font_size = 10;
  txt_params->font_params.font_color.red = 1.0;
  txt_params->font_params.font_color.green = 1.0;
  txt_params->font_params.font_color.blue = 1.0;
  txt_params->font_params.font_color.alpha = 1.0;

  // Text background color
  txt_params->set_bg_clr = 1;
  txt_params->text_bg_clr.red = 0.0;
  txt_params->text_bg_clr.green = 0.0;
  txt_params->text_bg_clr.blue = 0.0;
  txt_params->text_bg_clr.alpha = 1.0;

  nvds_add_display_meta_to_frame(frame_meta, display_meta);
}
